# Установка проекта локально со стороны frontend #

Необходимо установить

* nodejs
* bower
* grunt

```
#!bash

git clone git@bitbucket.org:ritmart/russian-centr.git path_to_project/
sudo apt-get update && sudo apt-get install nodejs npm -y
npm install bower -g
npm install grunt-cli -g
cd path_to_project/
bower install
npm install grunt-postcss --save-dev
npm install grunt-postcss pixrem autoprefixer cssnano postcss-color-rgba-fallback postcss-opacity
npm install grunt-contrib-uglify --save-dev
npm install grunt-contrib-watch --save-dev
npm install grunt-contrib-clean --save-dev
npm install grunt-spritesmith
npm install grunt-contrib-imagemin --save-dev
npm install grunt-concurrent --save-dev
npm install grunt-csscomb --save-dev
```